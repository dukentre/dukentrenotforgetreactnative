module.exports = {
    colors:{
        statusBarColor:"#C56A37",
        accent:"#F68545",
        background:"#FFFFFF",
        text:{
            primary: "#000000",
            secondary:"#515151",
        },
        priority:{
            1:"#64C16D",
            2:"#6083FE",
            3:"#FFC453",
            4:"#FC4040",
        }
    }
}
