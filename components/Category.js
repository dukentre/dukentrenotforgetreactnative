import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
const StyleConstants = require("../StyleConstants");

const Category = (props)=>{
    return (
        <View style={styles.mainView}>
            <Text style={styles.categoryNameText}>{props.name}</Text>
            <ScrollView>
                {props?.tasks?.map((task,index)=>(
                    <View key={index} style={styles.taskWrapper}>
                        <View style={{width:8,height:"100%",marginRight:8,backgroundColor:StyleConstants.colors.priority[task.priority]}}></View>
                        <View style={styles.taskContentWrapper}>
                            <View style={styles.taskTitleAndDesc}>
                                <Text style={styles.taskTitleText}>{task.title}</Text>
                                <Text style={styles.taskDescText}>{task.description}</Text>
                            </View>
                            <CheckBox
                                disabled={false}
                            />
                        </View>
                    </View>

                ))}
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    mainView:{
        flex:1,
        justifyContent:"flex-start",
        alignItems:"flex-start",
        flexDirection:'column',
        padding:12,
    },
    categoryNameText:{
        fontSize:18,
        color:StyleConstants.colors.text.secondary,
        marginBottom:12,
    },
    taskWrapper:{
        flexDirection: "row",
        marginTop: 4,
        marginBottom: 4,
    },
    taskContentWrapper:{
        width:"95%",
        flexDirection: "row",
        justifyContent:"space-between",
        alignItems:"center",
    },
    taskTitleAndDesc:{
        paddingVertical:8,
        maxWidth:"85%"
    },
    taskTitleText:{
        color:StyleConstants.colors.text.primary,
        fontSize:18
    },
    taskDescText:{
        fontSize:14,
        color:StyleConstants.colors.text.secondary
    }
})

export default Category;
