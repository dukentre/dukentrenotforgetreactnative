import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
} from 'react-native';
const StyleConstants = require("../StyleConstants");

const Header = ()=>{
    return (
        <View style={styles.headerView}>
            <Text style={styles.text}>Not Fogot! By Dukentre</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    headerView:{
        flex:1,
        justifyContent:"center",
        alignItems:"flex-start",
        padding:32,
        paddingLeft:12,
        backgroundColor:StyleConstants.colors.accent,
    },
    text:{
        fontSize:18,
        color:"#FFFFFF"
    }
})

export default Header;
