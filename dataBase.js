module.exports = {
    categories:[
        {
            name:"Работа",
            tasks:[
                {
                    priority:1,
                    title:"Погладить Герасимова",
                    description:"За то, что разрешил писать на нормальном языке",
                },
                {
                    priority:3,
                    title:"Попробовать хряпнуть пивка",
                    description:"Сибирская корона крепкое",
                }
            ]
        },
        {
            name:"Учёба",
            tasks:[
                {
                    priority:2,
                    title:"Проспать лабу",
                    description:"Не, ну а чё? Каждый день так делаю",
                },
                {
                    priority:4,
                    title:"Проспать все пары",
                    description:"Офк дистанционка",
                },
                {
                    priority:4,
                    title:"Закибербулить герасимова",
                    description:"Дело житейское",
                }
            ]
        },
        {
            name:"Житуха",
            tasks:[
                {
                    priority:2,
                    title:"Поесть",
                    description:"С самого утра не ел",
                },
                {
                    priority:1,
                    title:"Убраться",
                    description:"как-нибудь в другой раз",
                },
                {
                    priority:4,
                    title:"Закибербулить герасимова",
                    description:"И тут тоже это?",
                }
            ]
        }
    ]
}
