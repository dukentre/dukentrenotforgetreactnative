/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
} from 'react-native';

import Header from './components/Header';
import Category from './components/Category';

const StyleConstants = require('./StyleConstants');
const dataBase = require('./dataBase');
const App: () => React$Node = () => {
    return (
        <>
            <StatusBar backgroundColor={StyleConstants.colors.statusBarColor}/>
            <SafeAreaView style={styles.screenWrapper}>
                <Header/>
                <ScrollView
                    contentInsetAdjustmentBehavior="automatic"
                    style={styles.scrollView}>
                    {dataBase.categories.map((category,index) => (
                        <Category key={index} name={category.name} tasks={category.tasks}/>
                    ))}
                </ScrollView>
            </SafeAreaView>
        </>
    );
};

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: StyleConstants.colors.background,
    },
    screenWrapper:{
        maxHeight:"100%"
    }

});

export default App;
